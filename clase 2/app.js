const $template = document.querySelector('#temp-segment');
const genericStyles = {
    boundingbox: [-4, 2, 4, -2],
    grids: { x: { visible: false }, y: { visible: false } },
    axies: {
        y: {
            ticks: { visible: false },
            strokeColor: "green",
        },
        x: {
            ticks: { visible: false },
            strokeColor: "red",
        },
    },
}

const defBoard = {

    board_1: {
        styles: genericStyles,
        points: [
            { x: 1, y: 1, style: { visible: true } },
            [2, 1.5, true],
        ],
        inputs: [
            { x: -2, y: -1, value: '3', },
            { x: 0, y: 1, value: '1' },
            { x: 2, y: -1, value: '2' },
        ],

    },
};

/* hago una copia del template */
const cloneTmp = $template.content.firstElementChild.cloneNode(true);
/* cambio el id del board ya que necesito un id unico en este 
elemento para que la libreria mathlive peuda funcionar*/
cloneTmp.querySelector('#jxgbox').id = `board_1`

document.querySelector('.main').appendChild(cloneTmp)
// Crear una instancia de la clase baseBoards con el objeto literal definition
const baseBoardsX = new baseBoards({ id: `board_1`, ...defBoard[`board_1`] });
