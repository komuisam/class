const def = {
  artifacts: {
    artifact_1: { class: 'existo' },
    artifact_2: { inputs: [1, 2, 3] }
  }
};

function main(params) {
  const idTemplate = 'temp-segment';
  const fragment = new DocumentFragment();
  const temp = document.querySelector('#' + idTemplate);

  Object.entries(def.artifacts).map((element, index) => {

    const [key, value] = element;
    const id = key;
    const cloneTmp = temp.content.firstElementChild.cloneNode(true);

    if (value?.inputs) {
      value?.inputs.forEach((value, i) => {
        console.log(value);
        console.log(cloneTmp.querySelector('.value_' + (i + 1)));
        cloneTmp.querySelector('.value_' + (i + 1)).value = value;
      });
    }
    cloneTmp.id = id;
    fragment.appendChild(cloneTmp);
    MathLive.renderMathInDocument();
  });

  document.body.appendChild(fragment);
}

main();